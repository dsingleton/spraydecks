Readme for Spraydecks
==================

Importing Spraydecks requires using a custom dataflow profile, until I discover how to add these as part of the module
installation, the profile XML is listed here:

# Profile XML:

    <action type="dataflow/convert_adapter_io" method="load">
        <var name="type">file</var>
        <var name="path">var/import</var>
        <var name="filename"><![CDATA[spraydecks.csv]]></var>
        <var name="format"><![CDATA[csv]]></var>
    </action>
    <action type="dataflow/convert_parser_csv" method="parse">
        <var name="delimiter"><![CDATA[,]]></var>
        <var name="enclose"><![CDATA["]]></var>
        <var name="fieldnames">true</var>
        <var name="store"><![CDATA[0]]></var>
        <var name="decimal_separator"><![CDATA[.]]></var>
        <var name="adapter">dsingleton_spraydecks/convert_adapter_spraydeck</var>
        <var name="method">saveRow</var>
    </action>

## CSV File

This Profile XML works in conjunction with a CSV in the format:

code,brand,model,size,image,text

## Required filds

Technically only the code is required for the import, but in truth you should always include code, brand,model & size.