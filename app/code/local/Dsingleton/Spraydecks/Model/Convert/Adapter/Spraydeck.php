<?php
class Dsingleton_Spraydecks_Model_Convert_Adapter_Spraydeck
    extends Mage_Dataflow_Model_Convert_Adapter_Abstract
{
    protected $_finderModel;

    public function load() {
      // you have to create this method, enforced by Mage_Dataflow_Model_Convert_Adapter_Interface 
    }

    public function save() {
      // you have to create this method, enforced by Mage_Dataflow_Model_Convert_Adapter_Interface       
    }

    public function getFinderModel()
    {
        if (is_null($this->_finderModel)) {
            $finderModel = Mage::getModel('dsingleton_spraydecks/finder');
            $this->_finderModel = Mage::objects()->save($finderModel);
        }
        return Mage::objects()->load($this->_finderModel);
    }

    public function saveRow(array $importData)
    {
      $finder = $this->getFinderModel();

      if (empty($importData['code'])) {
          $message = Mage::helper('dsingleton_spraydecks')->__('Skip import row, required field "%s" not defined', 'code');
          Mage::throwException($message);
      }
      else
      {
        $finder->load($importData['code'],'code');
      }
	  
	  $finder->setCode($importData['code']);
      $finder->setBrand($importData['brand']);
      $finder->setModel($importData['model']);
      $finder->setSize($importData['size']);
      $finder->setImage($importData['image']);
      $finder->setText($importData['text']);

      $finder->save();

      return true;

    }
}