<?php
class Dsingleton_Spraydecks_Model_Finder extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {  
        $this->_init('dsingleton_spraydecks/finder');
    }  
}