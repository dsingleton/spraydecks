<?php
class Dsingleton_Spraydecks_Adminhtml_FinderController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {  
        // Let's call our initAction method which will set some basic params for each action
        $this->_initAction()
            ->renderLayout();
    }  
     
    public function newAction()
    {  
        // We just forward the new action to a blank edit form
        $this->_forward('edit');
    }  
     
    public function editAction()
    {  
        $this->_initAction();
     
        // Get id if available
        $id  = $this->getRequest()->getParam('id');
        $model = Mage::getModel('dsingleton_spraydecks/finder');
     
        if ($id) {
            // Load record
            $model->load($id);
     
            // Check if record is loaded
            if (!$model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('This kayak no longer exists.'));
                $this->_redirect('*/*/');
     
                return;
            }  
        }  
     
        $this->_title($model->getId() ? $model->getModel() : $this->__('New Kayak'));
     
        $data = Mage::getSingleton('adminhtml/session')->getFinderData(true);
        if (!empty($data)) {
            $model->setData($data);
        }  
     
        Mage::register('dsingleton_spraydecks', $model);
     
        $this->_initAction()
            ->_addBreadcrumb($id ? $this->__('Edit Kayak') : $this->__('New Kayak'), $id ? $this->__('Edit Kayak') : $this->__('New Kayak'))
            ->_addContent($this->getLayout()->createBlock('dsingleton_spraydecks/adminhtml_finder_edit')->setData('action', $this->getUrl('*/*/save')))
            ->renderLayout();
    }
     
    public function saveAction()
    {
        if ($postData = $this->getRequest()->getPost()) {
            $model = Mage::getSingleton('dsingleton_spraydecks/finder');
            $model->setData($postData);
 
            try {
                $model->save();
 
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The kayak has been saved.'));
                $this->_redirect('*/*/');
 
                return;
            }  
            catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('An error occurred while saving this kayak.'));
            }
 
            Mage::getSingleton('adminhtml/session')->setFinderData($postData);
            $this->_redirectReferer();
        }
    }
     
    public function messageAction()
    {
        $data = Mage::getModel('dsingleton_spraydecks/finder')->load($this->getRequest()->getParam('id'));
        echo $data->getContent();
    }
     
    /**
     * Initialize action
     *
     * Here, we set the breadcrumbs and the active menu
     *
     * @return Mage_Adminhtml_Controller_Action
     */
    protected function _initAction()
    {
        $this->loadLayout()
            // Make the active menu match the menu config nodes (without 'children' inbetween)
            ->_setActiveMenu('cms/dsingleton_spraydecks_finder');
            //->_title($this->__('CMS'))->_title($this->__('Spraydeck Finder'))
            //->_addBreadcrumb($this->__('CMS'), $this->__('CMS'))
            //->_addBreadcrumb($this->__('Spraydeck Finder'), $this->__('Spraydeck Finder'));
         
        return $this;
    }
     
    /**
     * Check currently called action by permissions for current user
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('cms/dsingleton_spraydecks_finder');
    }
}