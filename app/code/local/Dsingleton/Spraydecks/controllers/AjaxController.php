<?php
class Dsingleton_Spraydecks_AjaxController extends Mage_Core_Controller_Front_Action {

	/**
	 * Return the brands for a given manufacturer
	 */    
    public function brandAction()
    {
    	//check we have an ajax request
        $isAjax = Mage::app()->getRequest()->isAjax();
              
        if ($isAjax) {
  
        	$this->loadLayout(false);
			$this->renderLayout();
			
			$brand = $this->getRequest()->getParam('brand');
			
			if($brand)
			{
				$boat_model_collection = Mage::getModel('dsingleton_spraydecks/finder')
					->getCollection()
					->addFieldToSelect('code')
					->addFieldToSelect('model')
					->addFieldToFilter('brand', $brand)
					->load();
				
				$html = '';
				$model_list = array();
				$model_list['default'] = '-- Please Select --';
    	
				foreach ( $boat_model_collection as $boat_model )
				{
					$model_list[$boat_model->getData('code')] = $boat_model->getData('model');
				}
				
				$dropdown = '<option value="%s">%s</option>';
				
				
				 foreach ($model_list as $k => $v){
					 $html .= sprintf($dropdown, $k, $v);
				 }
				 
				 //return the options to Ajax Updater
				 $this->getResponse()->clearHeaders()->setHeader('Content-Type','text/html')->setBody($html);

			}
        }
    }
    
    public function modelAction()
    {
	    //check we have an ajax request
        $isAjax = Mage::app()->getRequest()->isAjax();
        
        if ($isAjax) {
  
        	$this->loadLayout(false);
			$this->renderLayout();
			
			$model = $this->getRequest()->getParam('model');
			
			if($model)
			{
				$boat = Mage::getModel('dsingleton_spraydecks/finder')
					->getCollection()
					->addFieldToFilter('code', $model)
					->setOrder('model','ASC')
					->getFirstItem();
				
				$html = '';
				
				if($boat)
				{
					
					$html.= '<p>Brand: ' . $boat->getData('brand') . '</p>';
					$html.= '<p>Model: ' . $boat->getData('model') . '</p>';
					$html.= '<p>Image: ' . $boat->getData('image') . '</p>';
					$html.= '<p>Size: ' . $boat->getData('size') . '</p>';
					$html.= '<p>Text: ' . $boat->getData('text') . '</p>';
					
					//var_dump($html);	
					
					//return the options to Ajax Updater
					$this->getResponse()->clearHeaders()->setHeader('Content-Type','text/html')->setBody($html);
				}
			}
        }
    }
}