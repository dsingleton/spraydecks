<?php
class Dsingleton_Spraydecks_Block_Adminhtml_Finder_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
         
        // Set some defaults for our grid
        $this->setDefaultSort('id');
        $this->setId('dsingleton_spraydecks_finder_grid');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
    }
     
    protected function _getCollectionClass()
    {
        // This is the model we are using for the grid
        return 'dsingleton_spraydecks/finder_collection';
    }
     
    protected function _prepareCollection()
    {
        // Get and set our collection for the grid
        $collection = Mage::getResourceModel($this->_getCollectionClass());
        $this->setCollection($collection);
         
        return parent::_prepareCollection();
    }
     
    protected function _prepareColumns()
    {
        // Add the columns that should appear in the grid
        $this->addColumn('id',
            array(
                'header'=> $this->__('ID'),
                'align' =>'right',
                'width' => '50px',
                'index' => 'id'
            )
        );
        
        $this->addColumn('code',
        	array(
        		'header'=> $this->__('Code'),
        		'width' => '100px',
        		'index' => 'code'
			)
		);
		
        $this->addColumn('brand',
            array(
                'header'=> $this->__('Brand'),
                'index' => 'brand'
            )
        );
        
        $this->addColumn('model',
        	array(
        		'header'=> $this->__('Model'),
        		'index' => 'model'
        	)
        );
        
        $this->addColumn('size',
        	array(
        		'header'=> $this->__('Size'),
        		'index' => 'size'
        	)
        );
         
        return parent::_prepareColumns();
    }
     
    public function getRowUrl($row)
    {
        // This is where our row data will link to
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}