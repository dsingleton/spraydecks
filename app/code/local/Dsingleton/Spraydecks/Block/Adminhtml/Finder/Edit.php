<?php
class Dsingleton_Spraydecks_Block_Adminhtml_Finder_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Init class
     */
    public function __construct()
    {  
        $this->_blockGroup = 'dsingleton_spraydecks';
        $this->_controller = 'adminhtml_finder';
     
        parent::__construct();
     
        $this->_updateButton('save', 'label', $this->__('Save Kayak'));
        $this->_updateButton('delete', 'label', $this->__('Delete Kayak'));
    }  
     
    /**
     * Get Header text
     *
     * @return string
     */
    public function getHeaderText()
    {  
        if (Mage::registry('dsingleton_spraydecks')->getId()) {
            return $this->__('Edit Kayak');
        }  
        else {
            return $this->__('New Kayak');
        }  
    }  
}