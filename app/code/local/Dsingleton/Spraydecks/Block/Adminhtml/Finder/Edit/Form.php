<?php
class Dsingleton_Spraydecks_Block_Adminhtml_Finder_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Init class
     */
    public function __construct()
    {  
        parent::__construct();
     
        $this->setId('dsingleton_spraydecks_finder_form');
        $this->setTitle($this->__('Kayak Information'));
    }  
     
    /**
     * Setup form fields for inserts/updates
     *
     * return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {  
        $model = Mage::registry('dsingleton_spraydecks');
        $helper = Mage::helper('dsingleton_spraydecks');
     
        $form = new Varien_Data_Form(array(
            'id'        => 'edit_form',
            'action'    => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
            'method'    => 'post'
        ));
     
        $fieldset = $form->addFieldset('base_fieldset', array(
            'legend'    => Mage::helper('dsingleton_spraydecks')->__('Kayak Information'),
            'class'     => 'fieldset-wide',
        ));
     
        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', array(
                'name' => 'id',
            ));
        }  
		
		$fieldset->addField('code', 'text', array(
			'name'		=> 'code',
			'label'		=> $helper->__('Code'),
			'title'		=> $helper->__('Code'),
			'required'	=>	true,
		));
		
        $fieldset->addField('brand', 'text', array(
            'name'      => 'brand',
            'label'     => $helper->__('Brand'),
            'title'     => $helper->__('Brand'),
            'required'  => true,
        ));
        
        $fieldset->addField('model', 'text', array(
            'name'      => 'model',
            'label'     => $helper->__('Model'),
            'title'     => $helper->__('Model'),
            'required'  => true,
        ));
        
        $fieldset->addField('size', 'text', array(
            'name'      => 'size',
            'label'     => $helper->__('Size'),
            'title'     => $helper->__('Size'),
            'required'  => true,
        ));
        
        $fieldset->addField('image', 'text', array(
        	'name'		=> 'image',
        	'label'		=> $helper->__('Image'),
        	'title'		=> $helper->__('Image'),
        	'required'	=> false,
        ));
        
        $fieldset->addField('text', 'editor', array(
        	'name'		=> 'text',
        	'label'		=> $helper->__('Text'),
        	'title'		=> $helper->__('Text'),
        	'required'	=> false,
        ));
     
        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);
     
        return parent::_prepareForm();
    }  
}