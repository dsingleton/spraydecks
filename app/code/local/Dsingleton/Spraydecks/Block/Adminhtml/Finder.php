<?php
class Dsingleton_Spraydecks_Block_Adminhtml_Finder extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        // The blockGroup must match the first half of how we call the block, and controller matches the second half
        // ie. dsingleton_spraydecks/adminhtml_finder
        $this->_blockGroup = 'dsingleton_spraydecks';
        $this->_controller = 'adminhtml_finder';
        $this->_headerText = $this->__('Spraydeck Finder');
         
        parent::__construct();
    }
}