<?php
class Dsingleton_Spraydecks_Block_Finder
    extends Mage_Core_Block_Template
    implements Mage_Widget_Block_Interface
{
    /**
     * A model to serialize attributes
     * @var Varien_Object
     */
    protected $_serializer = null;

    /**
     * Initialization
     */
    protected function _construct()
    {
        $this->_serializer = new Varien_Object();
        parent::_construct();
    }

    /**
     * Produce links list rendered as html
     *
     * @return string
     */
    protected function _toHtml()
    {
        $html = '';
        $list = $this->getBrands();
        $this->assign('list', $list);
        
        return parent::_toHtml();
    }
    
    private function getMageVersion()
    {
	    return Mage::getVersion();
    }
    
    private function getBrands()
    {
    	// Get distinct brands from our table -- brands should be seperated into a different table and normaized in future!!
    	$brands_collection = Mage::getModel('dsingleton_spraydecks/finder')
    		->getCollection()
    		->distinct(true)
    		->addFieldToSelect('brand')
    		->setOrder('brand', 'ASC');
    	$brands_collection->getSelect()->group('brand'); //required for 1.4.2
    	$brands_collection->load();
    		
    	//var_dump((string) $brands_collection->getSelect());
    	
    	// Build list for dropdown, there is definitely a better way to do this too!
    	$brand_list = array();
    	$brand_list[] = '-- Please Select --';
    	
    	foreach ( $brands_collection as $brand )
    	{
	    	$brand_list[] = $brand->getData('brand');
    	}
			
        return $brand_list;
	    
    }
}