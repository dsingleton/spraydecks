<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$tableName = $installer->getTable('dsingleton_spraydecks/finder');

if (!$installer->getConnection()->isTableExists($tableName))
{
	/**
	* Create table 'dsingleton_spraydecks_finder'
	*/
	$table = $installer->getConnection()
    	// The following call to getTable('dsingleton_spraydecks/finder') will lookup the resource for 
    	// dsingleton_spraydecks (dsingleton_spraydecks_mysql4), and look
		// for a corresponding entity called finder. The table name in the XML is
		// dsingleton_spraydecks_finder, so ths is what is created.
    	->newTable($installer->getTable('dsingleton_spraydecks/finder'))
		->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        	'identity'  => true,
			'unsigned'  => true,
			'nullable'  => false,
			'primary'   => true,
			), 'ID')
		->addColumn('code', Varien_Db_Ddl_Table::TYPE_VARCHAR, 0, array(
			'nullable' => false,
			), 'Code')
		->addColumn('brand', Varien_Db_Ddl_Table::TYPE_CLOB, 0, array(
        	'nullable'  => false,
			), 'Brand')
		->addColumn('model', Varien_Db_Ddl_Table::TYPE_CLOB, 0, array(
    		'nullable'  => false,
			), 'Model')
		->addColumn('size', Varien_Db_Ddl_Table::TYPE_CLOB, 0, array(
    		'nullable'  => false,
			), 'Size')
		->addColumn('image', Varien_Db_Ddl_Table::TYPE_CLOB, 0, array(
    		'nullable'  => true,
			), 'Image')
		->addColumn('text', Varien_Db_Ddl_Table::TYPE_CLOB, 0, array(
    		'nullable'  => true,
			), 'Text');

	$installer->getConnection()->createTable($table);
}

$installer->endSetup();